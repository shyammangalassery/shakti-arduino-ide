This repository is porting of Shakti on Arduino IDE 

## Guide

  * [Information](#information)
  * [Boards included](#boards-included-in-this-package)
  * [Setting up the ide](#setting-up-the-shakti-arduino-ide)
  * [Testing the sketches](#testing-the-sketches)
  * [Logging issue](#logging-issue)

  
  
## Information

This repository allows you to program SHAKTI supported development boards using the Arduino IDE.
This will be very useful to people, who are used to the Arduino environment.

## Boards included in this package 

1)Pinaka

2)Parashu

3)Artix7_35t

## Setting up the Shakti Arduino IDE

### Install Arduino IDE ###

Download and install Arduino IDE 1.8.9 or latest version tarball from the Arduino website. Unpack it and run their installation script as directed.

### Install SHAKTI supported boards ###


Add the Link below to the Additional Boards Manager URLs in the **Files->Preferences->Settings**.

https://gitlab.com/shaktiproject/software/shakti-arduino-ide/-/raw/master/package_shakti_index.json 


Open the Board Manager Dialog Box(**Tools->Board: -> Select Board Manager**)

In the Boards Manager Dialog Box, search the list and install **SHAKTI** boards.

### Setting up Arduino IDE for SHAKTI
Once the shakti board is installed. 

Go to **Tools->Board:->Shakti** and select the respective board.

Go to **Tools->Port** and select the port. 

Go to **Tools->Programmer** and select **Shakti OpenOCD Flash** or **Shakti OpenOCD RAM** based on the need. 

* Shakti OpenOCD Flash - To upload the program to the flash memory and program will be  still executed every time the board is reset. 

* Shakti OpenOCD RAM - To upload the program to RAM and the uploaded program would not be retained after a reset. 


## Testing the sketches 
Initiallly to test a demo sketch 

1)Go to **File->Examples->UART** and select **hello_world**

2)Click on the Verify icon (Tick icon on top left ) to compile the program. 

3)Open the Serial console using miniterm on a seperate terminal or any other application. 
> sudo miniterm.py /dev/ttyUSB1 19200

4)Click the Upload icon on Arduino IDE(Right arrow key next to verify ) to upload the program. 

5)Switch to serial console to see if the Program has been uploaded successfully.


## Logging Issue 

Any issue or clarification can be raised under issues. <br/>
Before raising an issue, please check if there are any similar issues.

Please follow the below steps to create a issue.

- Go to [`issues`](https://gitlab.com/shaktiproject/software/arduino-IDE-test/issues).
- After clicking on New Issue you will get an option to select a template.
- Click on choose template, list of available templates will be displayed, Select template "Bug".
- Once the template named 'Bug' is selected, the description text box is populated by the template.
- Please fill all the fields in the description textbox.

